import sys
import math

def xtoi(x, grid_dims, grid_min_vals, eta):
	idx = 0
	for i in range(len(x)-1,0,-1):
		idx = grid_dims[i-1]*(idx + round((x[i]-grid_min_vals[i])/eta[i]))
	idx += round((x[0]-grid_min_vals[0])/eta[0])
	return idx

def itox(idx, grid_dims, grid_min_vals, eta):
	x = [0] * len(grid_dims)
	for i in range(len(x)):
		x[i] = grid_min_vals[i] + (idx % grid_dims[i]) * eta[i]
		idx = idx // grid_dims[i]
	return x

# Arguments: controller file, output file name
def main():
	with open(sys.argv[1], 'r') as fp:
		lines = [x[:-1] for x in fp.readlines()]
	action_dict = {}
	# Skip to the start of encode3 section
	cur_idx = int(lines[0])+1
	# Reading encode3
	cur_size = int(lines[cur_idx])
	print(f"encode3 size: {cur_size}")
	encode3 = lines[cur_idx+1:cur_idx+cur_size]
	# Reading ctrlr
	cur_idx += cur_size+1
	cur_size = int(lines[cur_idx])
	print(f"ctrlr size: {cur_size}")
	ctrlr = lines[cur_idx+1: cur_idx+cur_size]
	# Reading ctrl
	cur_idx += cur_size+1
	cur_size = int(lines[cur_idx])
	print(f"ctrl size: {cur_size}")
	ctrl = lines[cur_idx+1: cur_idx+cur_size]

	x_lb = [0, 0, -3.5]
	x_ub = [10, 10, 3.5]
	x_eta = [0.2, 0.2, 0.2]
	
	x_grid_spec = list(zip(x_lb, x_ub, x_eta))
	x_grid_dims = [math.floor((y-x)/z)+1 for x,y,z in x_grid_spec]
	x_grid_min_vals = [x+((y-x)-(math.floor((y-x)/z))*z)/2 for x,y,z in x_grid_spec]

	u_lb = [-1, -1]
	u_ub = [1, 1]
	u_eta = [0.3, 0.3]
	
	u_grid_spec = list(zip(u_lb, u_ub, u_eta))
	u_grid_dims = [math.floor((y-x)/z)+1 for x,y,z in u_grid_spec]
	u_grid_min_vals = [x+((y-x)-(math.floor((y-x)/z))*z)/2 for x,y,z in u_grid_spec]

	# uint32_max
	uint32_max = 2**32 - 1
	out_lines = []
	for x,y in enumerate(encode3):
		ctrlr_idx = int(y)
		if ctrlr_idx != uint32_max:
			num_a,_,ctrl_idx = ctrlr[ctrlr_idx].split()
			ctrl_idx = int(ctrl_idx)
			num_a = int(num_a)
			x_val = [str(z) for z in itox(x, x_grid_dims, x_grid_min_vals, x_eta)]
			x_str = ' '.join(x_val)
			for i in range(int(ctrl_idx), ctrl_idx+num_a):
				q,u = ctrl[i].split()
				u_val = [str(z) for z in itox(int(u), u_grid_dims, u_grid_min_vals, u_eta)]
				out_lines.append(f"{x_str},{q},{' '.join(u_val)}\n")

	print(f"Total number of data points: {len(out_lines)}")
	with open(sys.argv[2], 'w+') as fp:
		fp.writelines(out_lines)






if __name__=='__main__':
	main()