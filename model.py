import torch
from torch import nn
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import lightning
from torchdyn import *
from torchdyn.core import NeuralODE
from torchdyn.datasets import *

# device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')
print("Cuda available: ",torch.cuda.is_available())
print("Current device: ",  torch.cuda.current_device())
torch.set_float32_matmul_precision('high')

class ROCSDataset(Dataset):

    def __init__(self, ds_file_name):
        super().__init__()
        with open(ds_file_name, 'r') as fp:
            self.data = [x[:-1] for x in fp.readlines()]

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        cur_data = self.data[index].split(',')
        x = [float(y) for y in cur_data[0].split()]
        x.append(int(cur_data[1]))
        q = [float(y) for y in cur_data[2].split()]
        # Uncomment for torchdyn (Neural ODE)
        # q.append(x[2]) 
        # q.append(x[3])
        return torch.Tensor(x), torch.Tensor(q)

class LinearNNCtrlr(lightning.LightningModule):

    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.layers = nn.Sequential(
            nn.Linear(input_dim, 32),
            nn.Tanh(),
            nn.Linear(32, 64),
            nn.Tanh(),
            nn.Linear(64, 128),
            nn.Tanh(),
            nn.Linear(128, 64),
            nn.Tanh(),
            nn.Linear(64, 32),
            nn.Tanh(),
            nn.Linear(32, output_dim)
        )

        self.train_losses = []
        self.train_running_loss = 0.
        self.num_train_step = 0

        self.val_losses = []
        self.val_running_loss = 0.
        self.num_val_step = 0


    def forward(self, x):
        return self.layers(x)

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.layers(x)
        loss = nn.MSELoss()(y_hat, y)
        self.train_running_loss += loss
        self.num_train_step += 1
        return {'loss': loss}
    
    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.layers(x)
        loss = nn.MSELoss()(y_hat, y)
        self.val_running_loss += loss
        self.num_val_step += 1
        return {'loss': loss}
    
    def on_train_epoch_end(self):
        self.train_losses.append(self.train_running_loss/self.num_train_step)
        self.val_losses.append(self.val_running_loss/self.num_val_step)
        self.train_running_loss = 0.
        self.num_train_step = 0
        self.val_running_loss = 0.
        self.val_train_step = 0
        print(f"\nTrain loss = {self.train_losses[-1]}, Validation loss = {self.val_losses[-1]}")
        return super().on_train_epoch_end()
    
    def configure_optimizers(self):
        return torch.optim.Adam(self.layers.parameters(), lr=0.01)
    

class Learner(lightning.LightningModule):
    def __init__(self, t_span:torch.Tensor, model:nn.ModuleDict):
        super().__init__()
        self.model, self.t_span = model, t_span
        self.train_losses = []
        self.train_running_loss = 0.
        self.num_train_step = 0

        self.val_losses = []
        self.val_running_loss = 0.
        self.num_val_step = 0

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        x, y = batch
        t_eval, y_hat = self.model(x, self.t_span)
        y_hat = y_hat[-1] # select last point of solution trajectory
        loss = nn.MSELoss()(y_hat, y)
        self.train_running_loss += loss
        self.num_train_step += 1
        return {'loss': loss}
    
    def validation_step(self, batch, batch_idx):
        x, y = batch
        t_eval, y_hat = self.model(x, self.t_span)
        y_hat = y_hat[-1] # select last point of solution trajectory
        loss = nn.MSELoss()(y_hat[:2], y[:2])
        self.val_running_loss += loss
        self.num_val_step += 1
        return {'loss': loss}
    
    def on_train_epoch_end(self):
        self.train_losses.append(self.train_running_loss/self.num_train_step)
        self.val_losses.append(self.val_running_loss/self.num_val_step)
        self.train_running_loss = 0.
        self.num_train_step = 0
        self.val_running_loss = 0.
        self.val_train_step = 0
        print(f"\nTrain loss = {self.train_losses[-1]}, Validation loss = {self.val_losses[-1]}")
        return super().on_train_epoch_end()

    def configure_optimizers(self):
        return torch.optim.Adam(self.model.parameters(), lr=0.01)

ds_file_name = "test_data.txt"
output_file_name = "model.pt"
ds = ROCSDataset(ds_file_name)
train_ds, val_ds = torch.utils.data.random_split(ds, [0.9, 0.1])
train_dl = DataLoader(train_ds, batch_size=32, shuffle=True)
val_dl = DataLoader(val_ds, batch_size=32, shuffle=False)
input_dim = 4
output_dim = 2

# f = nn.Sequential(
#     nn.Linear(4,32),
#     nn.Tanh(),
#     nn.Linear(32,64),
#     nn.Tanh(),
#     nn.Linear(64,32),
#     nn.Tanh(),
#     nn.Linear(32,4)
# )
# t_span = torch.linspace(0,1,5)
# model = NeuralODE(f, sensitivity='adjoint', solver='rk4', solver_adjoint='rk4')
# model = Learner(t_span, model)
model = LinearNNCtrlr(4,2)
# trainer = lightning.Trainer(max_epochs=2, callbacks=[EarlyStopping(monitor='val_loss', mode='min'), ModelCheckpoint(monitor='val_loss')], accelerator='auto', devices='auto', strategy='auto')
trainer = lightning.Trainer(max_epochs=10)
trainer.fit(model, train_dl, val_dl)

def car_dynamic(state,action,t,num_step):
    alpha = math.atan(math.tan(action[1]/2.))
    rhs = [lambda u,x: u[0]*math.cos(alpha+x[2])/math.cos(alpha),
           lambda u,x: u[0]*math.sin(alpha+x[2])/math.cos(alpha),
           lambda u,x: u[0]*math.tan(u[1])]
    h = t/num_step
    k = []
    state_copy = state.copy()
    for _ in range(num_step):
        k.append([x(action, state_copy) for x in rhs])
        state_copy = [x+h/2*y for x,y in list(zip(state, k[0]))]
        k.append([x(action, state_copy) for x in rhs])
        state_copy = [x+h/2*y for x,y in list(zip(state, k[1]))]
        k.append([x(action, state_copy) for x in rhs])
        state_copy = [x+h*y for x,y in list(zip(state, k[2]))]
        k.append([x(action, state_copy) for x in rhs])
        for i in range(len(state_copy)):
            state_copy[i] += (h/6)*(k[0][i]+2*k[1][i]+2*k[2][i]+k[3][i])
        k.clear()
    return state_copy


def xtoi(x, grid_dims, grid_min_vals, eta):
    idx = 0
    for i in range(len(x)-1, 0, -1):
        if x[i] < grid_min_vals[i]:
            return -1
        idx = grid_dims[i-1]*(idx+round((x[i]-grid_min_vals[i])/eta[i]))
    if x[0] < grid_min_vals[0]:
        return -1
    idx += round((x[0]-grid_min_vals[0])/eta[0])
    return idx



MAX_NUM_ITER = 100000
UINT32_MAX = 2**32 - 1
delta_t = 0.3
# Setting grid for xtoi
x_lb = [0, 0, -3.5]
x_ub = [10, 10, 3.5]
x_eta = [0.2, 0.2, 0.2]

x_grid_spec = list(zip(x_lb, x_ub, x_eta))
x_grid_dims = [math.floor((y-x)/z)+1 for x,y,z in x_grid_spec]
x_grid_min_vals = [x+((y-x)-(math.floor((y-x)/z))*z)/2 for x,y,z in x_grid_spec]

init_pos = [0.3, 0.3, 0.3]
init_state_idx = xtoi(init_pos, x_grid_dims, x_grid_min_vals, x_eta)
line_num, offset = divmod(init_state_idx+1, 8)
# the amount of times visiting accepting state needed for the success criteria
acc_goal = 5
# AP setup
num_ap = 3
goal = np.array([[1.0, 2.0, 0.5, 2.0],
                     [0.5, 2.5, 7.5, 8.5],
                     [7.1, 9.1, 4.6, 6.4]])
eta = [0.1, 0.1]
# AP lambda function, label is AP index
target_fn = lambda label,x: int(goal[label,0] <= x[0]-eta[0] and
                             x[0]+eta[0] <= goal[label,1] and
                             goal[label,2] <= x[1]-eta[1] and
                             x[1]+eta[1] <= goal[label,3])
# Read DBA transitions
with open('controller_car.txt','r') as fp:
    lines = [x[:-1] for x in fp.readlines()]
# check if initial position is in NTS winning set
temp = int(lines[line_num+1])
temp = temp >> (8-offset)
assert temp % 2 == 0, "The initial position is not in the NTS winning set"
# Extract encode3
cur_idx = int(lines[0])+1
encode3 = [int(x) for x in lines[cur_idx+1:cur_idx+int(lines[cur_idx])+1]]
# Extract ctrlr
cur_idx += int(lines[cur_idx])+1
ctrlr = [[int(y) for y in x.split()] for x in lines[cur_idx+1:cur_idx+int(lines[cur_idx])+1]]
# Extract ctrl
cur_idx += int(lines[cur_idx])+1
ctrl = [int(x.split()[0]) for x in lines[cur_idx+1:cur_idx+int(lines[cur_idx])+1]]
# Extract q_prime as dba_mat
cur_idx += int(lines[cur_idx])+1
dba_mat_size = int(lines[cur_idx])
dba_mat = np.array([int(x) for x in lines[cur_idx+1:cur_idx+dba_mat_size+1]])
dba_mat = np.reshape(dba_mat, (-1, 2**num_ap))

# Read DBA information
with open('dba3.txt', 'r') as fp:
    lines = fp.readlines()
# use acc_states as a temporary holder to extract init_dba_state and num_dba_state
acc_states = [int(x) for x in lines[0][:-1].split()]
num_dba_state = acc_states[0]
init_dba_state = acc_states[2]
acc_states = [bool(int(x)) for x in lines[1].split()]

# Dynamic function for updating vehicle position:
dynamic_fn = car_dynamic

# epoch = 1
# lr = 0.01
# model = LinearNNCtrlr(input_dim,output_dim)
# model.to(device)
# optim = torch.optim.Adam(model.parameters(), lr=0.1)
# loss_fcn = torch.nn.MSELoss(reduction='mean')
# train_loss = []
# val_loss = []
# for e in range(epoch):
#     cur_train_loss = 0
#     total_steps = 0
#     model.train()
#     for i, (x, q) in tqdm(enumerate(train_dl),desc=f"Training Epoch {e+1}/{epoch}:"):
#         x=x.to(device)
#         q=q.to(device)
#         output = model(x)
#         loss = loss_fcn(output, q)
#         cur_train_loss += loss.item()
#         loss.backward()
#         total_steps += 1
#     train_loss.append(cur_train_loss/total_steps)
    
#     cur_val_loss = 0
#     total_steps = 0
#     with torch.no_grad():
#         for i, (x, q) in tqdm(enumerate(val_dl), desc=f"Validating Epoch {e+1}/{epoch}:"):
#             x=x.to(device)
#             q=q.to(device)
#             output = model(x)
#             loss = loss_fcn(output, q)
#             cur_val_loss += loss.item()
#             total_steps += 1
#     val_loss.append(cur_val_loss/total_steps)
#     print(f"Training Loss: {train_loss[e]}, Validation Loss: {val_loss[e]}\n")

traj = []
acc_count = 0
cur_pos = init_pos.copy()
cur_pos.append(init_dba_state)
traj.append(cur_pos.copy())
exited_initial_state = False
success = False
with torch.no_grad():
    for i in range(MAX_NUM_ITER):
        x = torch.Tensor(cur_pos).to(device)
        # For torchdyn version
        # u = model(x)[1][1,:2].tolist()
        # For native pytorch version
        u = model(x).cpu().tolist()
        cur_pos[:3] = dynamic_fn(cur_pos[:3], u, delta_t, 10)
        cur_state_idx = xtoi(cur_pos[:3], x_grid_dims, x_grid_min_vals, x_eta)
        assert cur_idx > 0, f"Outside of NTS states. Simulation stopped at timestep {i}"
        assert encode3[cur_state_idx] != UINT32_MAX, f"Product winning set does not contain NTS state {cur_state_idx}. Simulation stopped at timestep {i}"
        cur_ctrl_pos = ctrlr[encode3[cur_state_idx]][2]
        cur_ctrl_num_a = ctrlr[encode3[cur_state_idx]][0]
        cur_label = 0
        for j in range(num_ap):
            cur_label = cur_label << 1
            cur_label += target_fn(j,x)
        cur_pos[3] = dba_mat[cur_pos[3], cur_label]
        assert cur_pos[3] in ctrl[cur_ctrl_pos:cur_ctrl_pos+cur_ctrl_num_a], f"Product winning set does not contain NTS and DBA state pair ({cur_state_idx},{cur_pos[3]}). Simulation stopped at timestep {i}"
        if not exited_initial_state and cur_pos[3] != init_dba_state:
            exited_initial_state = True
        traj.append(cur_pos.copy())
        if acc_states[cur_pos[3]] and exited_initial_state:
            acc_count += 1
        if acc_count >= acc_goal:
            print(f"Successfully visited accepting state {acc_count} times. Simulation stopped at timestep {i}")
            success = True
            break
if not success:
    print(f"Simulation exceeded iteration number limit")

traj = np.array(traj)
#TODO: Add plotting obstacles later
plt.figure()
plt.plot(traj[:, 0], traj[:, 1])
plt.plot(traj[0,0], traj[0,1], marker='^', markerfacecolor='r')
plt.plot(traj[-1,0], traj[-1,1], marker='v', markerfacecolor='g')
plt.show()


